/*
 * File:   m90e26.h
 * Author: Agatha Niwomugizi
 */

#ifndef M90E26_H
#define	M90E26_H

#include "pps.h"
#include "pwr.h"
#include "uart.h"
#include "base.h"
#include "pinmap.h"
#include "device.h"

typedef const struct
{
    void (*initialise) (void);
    device_interface_t device_m;
    pwr_interface_t pwr_m;
    pps_interface_t pps_m;
    gpio_interface_t gpio_m;
    uart_interface_t uart_m;
}m90e26_interface_t;

extern m90e26_interface_t m90e26;

void m90e26_write();
void m90e26_read(uint8_t data, uint8_t data_size);

#endif	/* M90E26_H */
