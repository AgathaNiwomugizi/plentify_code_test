/*
 * File:   pinmap.h
 * Author: Plentify Hardware Team
 */

#ifndef PINMAP_H
#define	PINMAP_H

#include <xc.h>

#define _PORT_A_BASE_ADDRESS            _PORTA_BASE_ADDRESS
#define _PORT_B_BASE_ADDRESS            _PORTB_BASE_ADDRESS
#define _PORT_C_BASE_ADDRESS            0xBF802DB0                                      // Base address masks are incorrect for Port C and D
#define _PORT_D_BASE_ADDRESS            _PORTC_BASE_ADDRESS

#define PINMAP_RP_DUMMY_CHANNEL           0xff

typedef enum
{
    pinmap_pps_select_reset     = 0,
    pinmap_pps_select_A0        = 1,
    pinmap_pps_select_A1        = 2,
    pinmap_pps_select_A2        = 3,
    pinmap_pps_select_A3        = 4,
    pinmap_pps_select_A4        = 5,
    pinmap_pps_select_A5        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_A6        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_A7        = 21,
    pinmap_pps_select_A8        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_A9        = 24,
    pinmap_pps_select_A10       = 22,
    pinmap_pps_select_A11       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_A12       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_A13       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_A14       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_A15       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_B0        = 6,
    pinmap_pps_select_B1        = 7,
    pinmap_pps_select_B2        = 8,
    pinmap_pps_select_B3        = 9,
    pinmap_pps_select_B4        = 10,
    pinmap_pps_select_B5        = 11,
    pinmap_pps_select_B6        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_B7        = 12,
    pinmap_pps_select_B8        = 13,
    pinmap_pps_select_B9        = 14,
    pinmap_pps_select_B10       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_B11       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_B12       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_B13       = 15,
    pinmap_pps_select_B14       = 16,
    pinmap_pps_select_B15       = 17,
    pinmap_pps_select_C0        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C1        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C2        = 19,
    pinmap_pps_select_C3        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C4        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C5        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C6        = 23,
    pinmap_pps_select_C7        = 20,
    pinmap_pps_select_C8        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C9        = 18,
    pinmap_pps_select_C10       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C11       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C12       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C13       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C14       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_C15       = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_D0        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_D1        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_D2        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_D3        = PINMAP_RP_DUMMY_CHANNEL,
    pinmap_pps_select_D4        = PINMAP_RP_DUMMY_CHANNEL,
} pinmap_pps_select_t;

typedef const struct
{
    volatile uint32_t*                  port_address;
    uint8_t                             pin;
    pinmap_analogue_channel_select_t    analogue_channel;
    pinmap_pps_select_t                 pps_pin;
}pinmap_pin_t;

#define mPIN(name, _port, _pin)        pinmap_pin_t name = {.port_address = (volatile uint32_t*) _PORT_##_port##_BASE_ADDRESS, .pin = _pin, .analogue_channel = pinmap_analogue_channel_select_##_port##_pin, .pps_pin  = pinmap_pps_select_##_port##_pin}

mPIN(m90e26_uart_tx, A, 0);
mPIN(m90e26_uart_rx, A, 1);

#endif	/* PINMAP_H */
