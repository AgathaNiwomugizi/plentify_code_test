/*
 * File:   m90e26.c
 * Author: Agatha Niwomugizi
 */

 #include "m90e26.h"
#include "gpio.h"

// Function prototypes
static void m90e26_initialise(void);

m90e26_interface_t m90e26 =
{
    .initialise = m90e26_initialise,
};

static void m90e26_initialise(void)
{
  //initialize the m90e26
  //device
  m90e26.device_m.unlock;
          
  //power
  pwr_interface_t pwr={.unlock_PMD, .enable_peripheral(
  pwr_peripheral_select_uart2)};
  m90e26.pwr_m =pwr;
  
  //pps
  pps_interface_t pps = {.unlock, .set_input_function(m90e26_uart_rx,
  pps_input_function_select_uart2_rx),.set_output_function(
  m90e26_uart_tx,pps_output_function_select_uart2_tx)};
  m90e26.pps_m =pps;
  
  //gpio
  gpio_interface_t gpio = {.initialise(m90e26_uart_tx,gpio_mode_digital_output_push_pull)
           ,.initialise(m90e26_uart_rx,gpio_mode_digital_input_pull_up)};  
  m90e26.gpio_m =gpio;
  
  //uart
  uart_initialise_t p_uart_initialisation_struct ={.baudrate= uart_baudrate_9600,
  .data_and_parity_select= uart_data_and_parity_select_8bit_no_parity,
  .clock_select=uart_clock_select_sysclk,.stop_bit_select=uart_stop_bit_select_1bit};
  
  uart_interface_t uart={.enable,.initialise(p_uart2, &p_uart_initialisation_struct),.run_during_sleep(p_uart2, true),
  .select_receive_interrupt_mode(p_uart2,uart_receive_interrupt_select_one_character_received),
  .select_transmit_interrupt_mode(p_uart2,uart_transmit_interrupt_select_all_characters_transmitted),
  .select_RTS_mode(p_uart2,uart_RTS_mode_select_flow_control),
  .select_flow_control_pins(p_uart2,uart_flow_control_pin_enable_rts_cts)};
  m90e26.uart_m =uart;
}


boolean_state_t m90e26_write(uint8_t* data,uint8_t data_size)
{
  if((!m90e26.uart_m.is_transmit_shift_register_empty)&&(!m90e26.uart_m.is_tx_module_active()))
    m90e26.uart_m.enable_tx_module;
    
  while((!m90e26.uart_m.is_transmit_shift_register_empty)&&(m90e26.uart_m.is_tx_module_active())){
      if(!m90e26.uart_m.transmit_string(p_uart2,data,data_size, 20000))
          m90e26.uart_m.send_break;
  }
  
  m90e26.uart_m.disable_tx_module;
}


boolean_state_t m90e26_read(uint8_t* data,uint8_t data_size)
{
    if(!m90e26.uart_m.is_receive_buffer_not_empty())
    m90e26.uart_m.enable_rx_module;
    
    while((!m90e26.uart_m.is_receive_buffer_not_empty())){
        if(!m90e26.uart_m.receive_string(p_uart2,data,data_size, 20000))
            m90e26.uart_m.send_break;
    }
    
    m90e26.uart_m.disable_rx_module;
}

