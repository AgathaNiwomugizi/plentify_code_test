/*
 * File:   uart.h
 * Author: Plentify Hardware Team
 */

#ifndef UART_H
#define	UART_H

#include "base.h"

// Pointers to each UART block
typedef volatile uint32_t* const uart_t;
extern uart_t p_uart1;
extern uart_t p_uart2;
extern uart_t p_uart3;

typedef enum
{
    uart_clock_select_pbclk,
    uart_clock_select_sysclk,
    uart_clock_select_frc,
    uart_clock_select_refo1,
}uart_clock_select_t;

typedef enum
{
    uart_RTS_mode_select_flow_control,
    uart_RTS_mode_select_simplex
}uart_RTS_mode_select_t;

typedef enum
{
    uart_flow_control_pin_enable_none,
    uart_flow_control_pin_enable_rts,
    uart_flow_control_pin_enable_rts_cts,
    uart_flow_control_pin_enable_bclk,
}uart_flow_control_pin_enable_t;

typedef enum
{
    uart_baudrate_speed_mode_standard,
    uart_baudrate_speed_mode_high
}uart_baudrate_speed_mode_t;

typedef enum
{
    uart_data_and_parity_select_8bit_no_parity,
    uart_data_and_parity_select_8bit_even_parity,
    uart_data_and_parity_select_8bit_odd_parity,
    uart_data_and_parity_select_9bit_no_parity,
}uart_data_and_parity_select_t;

typedef enum
{
    uart_transmit_interrupt_select_one_empty_space,
    uart_transmit_interrupt_select_all_characters_transmitted,
    uart_transmit_interrupt_select_buffer_empty,
}uart_transmit_interrupt_select_t;

typedef enum
{
    uart_stop_bit_select_1bit,
    uart_stop_bit_select_2bit
}uart_stop_bit_select_t;

typedef enum
{
    // TODO Set this up for 8-level deep FIFO
    uart_receive_interrupt_select_one_character_received,
    uart_receive_interrupt_select_three_characters_received,
    uart_receive_interrupt_select_four_characters_received,
}uart_receive_interrupt_select_t;

typedef enum
{
    uart_baudrate_9600 = 9600,
    uart_baudrate_115200 = 115200,
    uart_baudrate_250000 = 250000,
    uart_baudrate_500000 = 500000,
}uart_baudrate_t;

typedef struct
{
    uart_clock_select_t                 clock_select;
    uart_data_and_parity_select_t       data_and_parity_select;
    uart_stop_bit_select_t              stop_bit_select;
    uart_baudrate_t                     baudrate;
}uart_initialise_t;

typedef const struct
{
    void (*enable) (uart_t p_uartx);
    void (*disable) (uart_t p_uartx);
    void (*enable_rx_module) (uart_t p_uartx);
    void (*disable_rx_module) (uart_t p_uartx);
    void (*enable_tx_module) (uart_t p_uartx);
    void (*disable_tx_module) (uart_t p_uartx);
    void (*run_during_sleep) (uart_t p_uartx, boolean_state_t state);
    boolean_state_t (*is_tx_module_active) (uart_t p_uartx);
    void (*select_clock) (uart_t p_uartx, uart_clock_select_t mode);
    void (*select_RTS_mode) (uart_t p_uartx, uart_RTS_mode_select_t mode);
    void (*select_flow_control_pins) (uart_t p_uartx, uart_flow_control_pin_enable_t mode);
    void (*select_baudrate_speed_mode) (uart_t p_uartx, uart_baudrate_speed_mode_t mode);
    void (*select_data_and_parity) (uart_t p_uartx, uart_data_and_parity_select_t mode);
    void (*select_stop_bits) (uart_t p_uartx, uart_stop_bit_select_t mode);
    void (*select_transmit_interrupt_mode) (uart_t p_uartx, uart_transmit_interrupt_select_t mode);
    void (*select_receive_interrupt_mode) (uart_t p_uartx, uart_receive_interrupt_select_t mode);
    void (*send_break) (uart_t p_uartx);
    boolean_state_t (*is_transmit_buffer_full) (uart_t p_uartx);
    boolean_state_t (*is_transmit_shift_register_empty) (uart_t p_uartx);
    boolean_state_t (*is_receiver_idle) (uart_t p_uartx);
    boolean_state_t (*is_receive_buffer_not_empty) (uart_t p_uartx);
    void (*transmit_byte) (uart_t p_uartx, uint8_t data, uint32_t per_byte_timeout_microseconds);                       // must enable system timer
    boolean_state_t (*transmit_string) (uart_t p_uartx, uint8_t* data, uint8_t data_size, uint32_t per_byte_timeout_microseconds);// must enable system timer
    boolean_state_t (*receive_string) (uart_t p_uartx, uint8_t* data, uint8_t data_size, uint32_t per_byte_timeout_microseconds);// must enable system timer
    void (*set_baudrate) (uart_t p_uartx, uart_baudrate_t baudrate);
    void (*initialise) (uart_t p_uartx, uart_initialise_t* p_uart_initialisation_struct);
}uart_interface_t;
extern uart_interface_t uart;

#endif	/* UART_H */
