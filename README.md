# Plentify Code Test

## Task
Write a basic communication driver for the Atmel M90E26 Single-Phase Energy Metering IC. The driver must be able to read and write from the M90E26 using UART communication. No additional functions are required.

You will be provided with a UART driver as well as any additional hardware abstraction that may be required.

Your code must be modular and maintainable as possible. You must adhear to the Barr Group coding standard. You must expose your public functions using the struct interface, a template has been provided.

Please fork this repository and work within your fork. Establish and use a "git flow" work flow, using atomic commits. Your use of git will be assessed.

Please feel free to ask questions and to use the infinite expanses of Google

### Specifications
* PIC32MM0256GPM064 Microcontroller
* Use UART 2, the Microcontroller pins are:
 * TX - RA0
 * RX - RA1
* UART speed - 9600bps

## Resources
* Barr Group Coding Standard - https://barrgroup.com/Embedded-Systems/Books/Embedded-C-Coding-Standard
* Gitflow - https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
* PIC32MM Datasheet - http://ww1.microchip.com/downloads/en/DeviceDoc/60001387c.pdf
* M90E26 Datasheet - http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-46002-SE-M90E26-Datasheet.pdf
